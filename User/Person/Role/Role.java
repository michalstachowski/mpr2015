package Person.Role;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import Person.Role.Permission.Permission;

public class Role {

    private String role_name;
    private Permission permissions[];

    /**
     * Constructor.
     * @param role_name - role name.
     */
    public Role(String role_name, String[][] permission){

        this.role_name = role_name;
        this.permissions = new Permission[permission.length];

        for (int i=0; i<permission.length; i++)
            if (permission[i].length == 2)
                this.permissions[i] = new Permission(permission[i][0], permission[i][1]);
    }

    /**
     * Get permissions for current role
     * @return role and list of permissions
     */
    public String[] get_roles_permissions(){
        List<String> permission_list = new ArrayList<String>();

        for (int i=0; i<this.permissions.length; i++)
            permission_list.add(this.permissions[i].permission_name + ": " + this.permissions[i].permission_desc);
        
        return new String[] {this.role_name, permission_list.toString()};
    }
}
