package Person.Role.Permission;

import java.util.HashMap;
import java.util.Map;

public class Permission {
    public final String permission_name;
    public final String permission_desc;

    /**
     * Constructor for
     * @param permission_name
     * @param permission_desc
     */
    public Permission(String permission_name, String permission_desc){
        this.permission_desc = permission_desc;
        this.permission_name = permission_name;
    }
}
