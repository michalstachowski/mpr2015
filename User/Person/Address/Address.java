package Person.Address;


public class Address {

    private String street;
    private int h_no;
    private int a_no = -1;
    private String zip_code;
    private String city;
    private String country;

    /**
     * Constructor without required apartment number
     * @param street
     * @param h_no - home number
     * @param zip_code
     * @param city
     * @param country
     */
    public Address(String street, int h_no, String zip_code, String city, String country){
        this.street = street;
        this.h_no = h_no;
        this.zip_code = zip_code;
        this.city = city;
        this.country = country;
    }

    /**
     * Constructor with requirement apartment number
     * @param street
     * @param h_no - home number
     * @param a_no - apartment number
     * @param zip_code
     * @param city
     * @param country
     */
    public Address(String street, int h_no, int a_no, String zip_code, String city, String country){
        this(street, h_no, zip_code, city, country);
        this.a_no = a_no;
    }

    /**
     * Shows full address
     */
    public void show_address_short() {
        System.out.println(this.get_address_short());
    }

    /**
     * Get String of full address
     * @return
     */
    public String get_address_short() {
        String apartment = "-";
        if (this.a_no!=-1)
            apartment = Integer.toString(this.a_no);
        return "[" + this.street + " street " + Integer.toString(this.h_no) + "/" + apartment +
                " " + this.zip_code + " " + this.city + " " + this.country + "]";
    }
}
