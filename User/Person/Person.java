package Person;

import Person.Address.Address;
import Person.Role.Role;

import java.security.*;

public class Person {

    private String login;
    private String name;
    private String surename;
    private String password;
    private int[] numbers;
    private Role[] roles;
    private Address address;

    /**
     * Constructor. Fill class content
     * @param login - User login
     * @param name - User name
     * @param surename - User surename
     * @param password - User password(as a plain text). It's changing into MD5 hashsum meanwhile
     * @param numbers - list of phone numbers
     * @param role_name - assigned roles
     * @param permission - assigned to roles pairs of permissions
     * @param street
     * @param h_no - house number
     * @param city
     * @param country
     */
    public Person(String login, String name, String surename, String password, int[] numbers, String role_name[],
                       String[][][] permission, String street, int h_no, String zip_code, String city, String country) {

        this.login = login;
        this.name = name;
        this.surename = surename;
        this.password = hash_password(password);
        this.numbers = numbers;

        this.address = new Address(street, h_no, zip_code, city, country);

        this.roles = new Role[role_name.length];

        for (int role=0; role<role_name.length; role++)
            if (role < permission.length)
                this.roles[role] = new Role(role_name[role], permission[role]);
            else
                this.roles[role] = new Role(role_name[role], new String[][] {{}});
    }

    /**
     * Constructor. Fill class content
     * @param login - User login
     * @param name - User name
     * @param surename - User surename
     * @param password - User password(as a plain text). It's changing into MD5 hashsum meanwhile
     * @param numbers - list of phone numbers
     * @param role_name - assigned roles
     * @param permission - assigned to roles pairs of permissions
     * @param street
     * @param a_no - apartment number
     * @param h_no - house number
     * @param city
     * @param country
     */
    public Person(String login, String name, String surename, String password, int[] numbers, String role_name[],
                       String[][][] permission, String street, int a_no, int h_no, String zip_code, String city,
                       String country) {

        this(login, name, surename, password, numbers, role_name, permission, street, h_no, zip_code, city, country);
        this.address = new Address(street, h_no, a_no, zip_code, city, country);
    }


    /**
     * Returns login
     * @return login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Returns full user information if given password match
     * @param password - given password
     */
    public void get_Data(String password){
        if (this.password.equals(hash_password(password))) {
            System.out.println("USER INFORMATION");
            System.out.println(" * Your login: " + this.login);
            System.out.println(" * Your name: " + this.name);
            System.out.println(" * Your surename: " + this.surename);
            System.out.println(" * Your address: " + this.address.get_address_short());
            System.out.println(" * Your numbers:");
            for (Integer number : this.numbers)
                System.out.println("\t" + number);

            System.out.println(" * Your roles");
            for (Role role : this.roles) {
                System.out.println("\t" + role.get_roles_permissions()[0]);
                System.out.println("\t\t" + role.get_roles_permissions()[1]);
            }
        }else {
            System.out.println("WRONG PASSWORD");
        }
    }

    /**
     * Hash password
     * @param password - plain text password
     * @return - hash form of password
     */
    private String hash_password(String password){
        try {
            MessageDigest hash_alg = MessageDigest.getInstance("SHA-256");
            hash_alg.update(password.getBytes());
            byte bytes[] = hash_alg.digest();
            StringBuffer sb = new StringBuffer();
            for (byte gbyte : bytes)
                sb.append(Integer.toString((gbyte & 0xff) + 0x100, 16).substring(1));
            return sb.toString();
        }catch (java.security.NoSuchAlgorithmException e) {
            return password;
        }
    }

}
