package common.CRUD;

/**
 * Created by Michal on 26/11/2015.
 */
public class Delete extends SqlBase{

    private String deleteStatement;

    public Delete(String tableName, String bufferedSchema){
        super(tableName, bufferedSchema);
    }

    @Override
    void prepareQueries() {
        deleteStatement = "DELETE FROM " + getTableName() + " WHERE (%CONDITION%)";
    }

    public void deleteFromTable(String where, String what){
        deleteStatement = deleteStatement.concat(createConditionStatement(where, what));
        execute(deleteStatement);
    }
}
