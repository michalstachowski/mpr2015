package common.CRUD;

/**
 * Created by Michal on 26/11/2015.
 */
public class Update extends SqlBase{

    private String updateStatement;

    public Update(String tableName, String bufferedSchema){
        super(tableName, bufferedSchema);
    }

    @Override
    void prepareQueries() {
        updateStatement = "UPDATE " + getTableName() + " SET ";
    }

    public void updateRow(String[] cols, String[] values, String where_column, String where_value){
        String setString = "";

        //DRY vulnerable!!!! See into Create class
        //Math.min protects whole statement against SQL syntax error(wrong number declared columns to values; SQL
        //databases doesn't require fulfill each column due this statement - it depends on schema of current schema)
        int maxIterationNumber = Math.min(cols.length, values.length);

        //Loop creates main parts of UPDATE statement
        for (int i=0; i<maxIterationNumber; i++){
            String separator = ", ";
            if (i == maxIterationNumber - 1){
                separator = "";
            }
            setString = setString.concat(cols[i] + "=" + values[i] + separator);
        }

        updateStatement = updateStatement.concat(setString);
        updateStatement = updateStatement.concat(createConditionStatement(where_column, where_value));

        execute(updateStatement);
    }


}
