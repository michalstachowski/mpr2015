package common.CRUD;

import java.sql.ResultSet;

/**
 * Created by Michal on 25/11/2015.
 */
public class Read extends SqlBase{

    private String selectStatement;

    public Read(String tableName, String tableSchema){
        super(tableName, tableSchema);
    }

    @Override
    void prepareQueries() {
        selectStatement = "SELECT * FROM " + getTableName();
    }

    /*
    Get all data from selected table and return fulfill ResultSet object
     */
    public ResultSet getAllData(){
        return executeQuery(selectStatement);
    }
}