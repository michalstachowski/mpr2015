package common.CRUD;

import java.sql.*;

/**
 * Created by Michal on 25/11/2015.
 */

public abstract class SqlBase {

    private Connection conn;
    private Statement stat;
    private String createTable;
    private String tableName;

    public SqlBase(String tableName, String bufferedSchema){
        this.tableName = tableName;
        prepareConnection();
        prepareQueriesCommon(bufferedSchema);
        prepareTable();
    }
    /*
    Prepare connection. Connection string is hardcoded, because it's in memory database and It don't have to be named
    by user.
     */
    public void prepareConnection(){
        try {
            this.conn = DriverManager.getConnection("jdbc:hsqldb:mem:runtime.db", "SA", "");
            this.stat = conn.createStatement();
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
            System.exit(1);
        }
    }

    protected void execute(String query){
        try {
            this.stat.execute(query);
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
        }
    }

    protected ResultSet executeQuery(String query){
        ResultSet resultSet = null;
        try {
            resultSet = this.stat.executeQuery(query);
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
        }
        return resultSet;
    }

    private void prepareQueriesCommon(String bufferedSchema){
        createTable = bufferedSchema;
        prepareQueries();
    }

    private boolean isTableExists(){
        try{
            String query  = "SELECT * FROM " + this.getTableName();
            return this.stat.execute(query);
        }catch (SQLException sqlerr){
            return false;
        }

    }

    private void prepareTable(){
        if (isTableExists()){
            try {
                this.stat.execute(this.createTable);
            }catch (SQLException sqlerr){
                sqlerr.printStackTrace();
            }
        }
    }

    /*
    Prepare condition statement
    return: String conditionStatement
     */
    protected String createConditionStatement(String where, String what){
        return " WHERE(" + where + "=" + what + ")";
    }


    protected String getTableName(){
        return this.tableName;
    }

    abstract void prepareQueries();

    protected void closeConnection() throws SQLException{
        if (!this.conn.isClosed()) {
                this.conn.close();
        }else{
            System.out.println("SQL connection is already closed");
        }
    }

    /*
    Overriding GC flow. Closing connections to Database
     */
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.closeConnection();
    }

}
