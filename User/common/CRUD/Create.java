package common.CRUD;

import java.sql.ResultSet;

/**
 * Created by Michal on 25/11/2015.
 */

public class Create extends SqlBase{

    private String insertData;

    public Create(String tableName, String bufferedSchema){
        super(tableName, bufferedSchema);
    }

    @Override
    void prepareQueries() {
        insertData = "INSERT INTO " + this.getTableName();
    }

    public void insertData(String[] columns, String[] values){

        String statementCols = "";
        String statementValues = "";

        //Math.min protects whole statement against SQL syntax error(wrong number declared columns to values; SQL
        //databases doesn't require fulfill each column due this statement - it depends on schema of current schema)
        int maxIterationNumber = Math.min(columns.length, values.length);

        //Loop creates main parts of INSERT statement
        for (int i=0; i<maxIterationNumber; i++){
            String separator = ", ";
            if (i == maxIterationNumber - 1){
                separator = "";
            }
            statementCols = statementCols.concat(columns[i] + separator);
            statementValues = statementValues.concat(values[i] + separator);
        }

        //Prepare last form of INSERT statement
        insertData = insertData.concat("(" + statementCols + ") VALUES (" + statementValues + ")");

        //Execute INSERT statement
        execute(insertData);
    }

}
