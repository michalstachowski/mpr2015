// TODO: Import person class

import Person.Person;

public class User {

    public static void main(String argz[]) {

        System.out.println("Zadanie 2");
        ex_2();
    }

    private static void ex_2(){
        System.out.println("EXERCISE 2");
        System.out.println("FULFILLING DATA");

        int[] numbers = new int[] {123456789};
        String[] roles = new String[] {"Administrator", "Employee"};
        String[][][] permissions = new String[][][] {{{"Root", "Access everywhere inside Operating system"},
                {"Network manager", "Access to network infrastructure"}}, {{"Basic", "Main gate and parking gate"}}};
        Person person = new Person("mstachow", "Michal", "Stachowski", "ciastko", numbers, roles, permissions,
                "Walowa", 7, "00-123", "Gdansk", "Poland");

        System.out.println("\nGET ACTIVE OBJECT LOGIN");
        System.out.println(person.getLogin());
        System.out.println("\nTRYING TO LOGIN WITH WRONG PASSWORD");
        person.get_Data("ciastka");
        System.out.println("\nTRYING TO LOGIN WITH CORRECT PASSWORD");
        person.get_Data("ciastko");

    }

}
