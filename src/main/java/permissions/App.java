package permissions;

import permissions.db.DAO.HsqlPersonRepository;
import permissions.db.common.PagingInfo;
import permissions.db.catalogs.RepositoryCatalog;
import permissions.db.DAO.HsqlRepositoryCatalog;
import permissions.domain.Person;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

		RepositoryCatalog catalogOf = new HsqlRepositoryCatalog();
		catalogOf.people().withName("jan", new PagingInfo());
		System.out.println( "Hello World!" );

		HsqlPersonRepository hsqlPersonRepository = new HsqlPersonRepository();

//	        List<Person> allPersons = hsqlPersonRepository.allOnPage();

//	        for(Person p : allPersons){
//	        	System.out.println(p.getId()+" "+p.getName()+" "+p.getSurname());
//	        }

		Person toUpdate = new Person();

		toUpdate.setName("Bolesław");
		toUpdate.setSurname("Prus");
		toUpdate.setId(2);

		hsqlPersonRepository.modify(toUpdate);

    }
}
