package permissions.db.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Michal on 27/11/2015.
 */
public abstract class SQLOperations {

    protected Connection connection;
    private String url = "jdbc:hsqldb:hsql://localhost/workdb";

    private void prepareConnection() throws SQLException {
        connection = DriverManager.getConnection(url);
    }

    private void closeConnection(){
        try {
            connection.close();
        }catch (SQLException sql_error){
            sql_error.printStackTrace();
        }
    }

    protected SQLOperations() {
        try {
            prepareConnection();
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
        }

    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        closeConnection();
    }
}
