package permissions.db.common;

import permissions.domain.DomainBase;

import java.sql.*;

/**
 * Created by Michal on 27/11/2015.
 */

public abstract class HsqlBaseRepository extends SQLOperations{

    protected String selectByIdSql ="SELECT * FROM " + getTableName() + " WHERE id=?";
    private String selectSql = "SELECT * FROM " + getTableName();
    private String deleteSql = "DELETE FROM " + getTableName() + " WHERE ?=?";
    protected String insertSql = "INSERT INTO " + getTableName() + " (?) VALUES (?)";
    protected String updateSql = "UPDATE " + getTableName() + " SET (?) WHERE ?=?";

    private PreparedStatement createTable;
    private PreparedStatement select;
    private PreparedStatement delete;
    protected PreparedStatement insert;
    protected PreparedStatement update;
    protected PreparedStatement selectById;

    protected abstract void createSpecificStatements() throws SQLException;
    protected abstract String getTableName();
    protected abstract String getTableDefinition();

    protected HsqlBaseRepository(){
        super();
        try{
            createStatements();
            createTableIfNeeded();
        }catch (SQLException sql_error){
            sql_error.printStackTrace();
        }
    }

    protected ResultSet getWithId(int id) throws SQLException {
        selectById.setInt(1, id);
        return selectById.executeQuery();
    }

    private void createStatements() throws SQLException{
        select = connection.prepareStatement(selectSql);
        delete = connection.prepareStatement(deleteSql);
        insert = connection.prepareStatement(insertSql);
        update = connection.prepareStatement(updateSql);
        selectById = connection.prepareStatement(selectByIdSql);

        createSpecificStatements();
    }



    private void createTableIfNeeded() throws SQLException{
        if (checkDBstatus()) {
            createTable = connection.prepareStatement(getTableDefinition());
            createTable.execute();
        }
    }

    private boolean checkDBstatus() throws SQLException{
        ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
        while(rs.next())
        {
            if(rs.getString("TABLE_NAME").equalsIgnoreCase(getTableName())){
                return true;
            }
        }
        return false;
    }

    public ResultSet getAll() throws SQLException{
        return select.executeQuery();
    }

    protected void remove_common(DomainBase domainBase){
        try {
            delete.setString(1, "id");
            delete.setInt(2, domainBase.getId());
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void add_common() throws SQLException{
        insert.executeUpdate();
    }

    protected void modify_common() throws SQLException{
        update.executeUpdate();
    }
}