package permissions.db.repositories;

import permissions.db.common.PagingInfo;
import permissions.domain.UserRoles;

import java.util.List;

/**
 * Created by Michal on 27/11/2015.
 */
public interface UserRolesRepository extends Repository<UserRoles> {
    public List<UserRoles> withUserID(int user_id, PagingInfo page);
    public List<UserRoles> withRoleID(int role_id, PagingInfo page);
}
