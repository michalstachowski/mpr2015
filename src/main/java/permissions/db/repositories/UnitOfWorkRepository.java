package permissions.db.repositories;

import permissions.domain.Entity;

/**
 * Created by mstachow on 11/27/2015.
 */
public interface UnitOfWorkRepository<TEntity> {
    public void persistAdd(TEntity entity);
    public void persistDelete(TEntity entity);
    public void persistUpdate(TEntity entity);
}
