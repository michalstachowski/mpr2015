package permissions.db.repositories;

import permissions.db.common.PagingInfo;
import permissions.domain.RolesPermissions;
import permissions.domain.UserRoles;

import java.util.List;

/**
 * Created by Michal on 27/11/2015.
 */
public interface RolesPermissionsRepository extends Repository<RolesPermissions> {
    public List<RolesPermissions> withPermissionID(int permission_id, PagingInfo page);
    public List<RolesPermissions> withRoleID(int role_id, PagingInfo page);
}
