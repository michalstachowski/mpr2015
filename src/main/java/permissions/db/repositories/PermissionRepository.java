package permissions.db.repositories;

import permissions.db.common.PagingInfo;
import permissions.domain.Permission;

import java.util.List;

/**
 * Created by Michal on 27/11/2015.
 */
public interface PermissionRepository extends Repository<Permission>{
    public List<Permission> withName(String name, PagingInfo page);
}
