package permissions.db.repositories;

import permissions.db.common.PagingInfo;
import permissions.domain.Address;

import java.util.List;

/**
 * Created by Michal on 27/11/2015.
 */
public interface AddressRepository extends Repository<Address> {
    public List<Address> withCountry(String country, PagingInfo page);
    public List<Address> withCity(String city, PagingInfo page);
    public List<Address> withStreet(String street, PagingInfo page);
    public List<Address> withPostalCode(String postalCode, PagingInfo page);
    public List<Address> withHouseNumber(String houseNumber, PagingInfo page);
    public List<Address> withLocalNumber(String LocalNumber, PagingInfo page);
}
