package permissions.db.repositories;

import permissions.db.common.PagingInfo;
import permissions.domain.Role;
import java.util.List;

/**
 * Created by Michal on 27/11/2015.
 */
public interface RolesRepository extends Repository<Role> {
    public List<Role> withName(String name, PagingInfo page);
}
