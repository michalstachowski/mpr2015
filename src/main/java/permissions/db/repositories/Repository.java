package permissions.db.repositories;

import permissions.db.common.PagingInfo;

import java.util.List;

public interface Repository<Entity> {

	public Entity withId(int id);
	public List<Entity> allOnPage(PagingInfo page);
	public void add(Entity entity);
	public void modify(Entity entity);
	public void remove(Entity entity);
}
