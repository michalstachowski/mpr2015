package permissions.db.repositories;

import permissions.domain.Entity;
import permissions.domain.EnumerationValue;

import java.util.List;

/**
 * Created by mstachow on 11/27/2015.
 */
public interface EnumerationValueRepository extends Repository<EnumerationValue> {
    public List<EnumerationValue> withName(String name);
    public List<EnumerationValue> withIntKey(int key, String name);
    public List<EnumerationValue> withStringKey(String key, String name);
}
