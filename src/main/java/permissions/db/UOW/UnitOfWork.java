package permissions.db.UOW;

import permissions.db.repositories.UnitOfWorkRepository;
import permissions.domain.Entity;

/**
 * Created by Michal on 27/11/2015.
 */
public interface UnitOfWork {
    public void saveChanges();
    public void undo();
    public void markAsNew(Entity entity, UnitOfWorkRepository repository);
    public void markAsDeleted(Entity entity, UnitOfWorkRepository repository);
    public void markAsChanged(Entity entity, UnitOfWorkRepository repository);
}
