package permissions.db.catalogs;

import permissions.db.repositories.*;

public interface RepositoryCatalog {
	
	public PersonRepository people();
	public EnumerationValueRepository enumeration();
	public UserRepository user();
	public PermissionRepository permission();
	public RolesRepository roles();
}
