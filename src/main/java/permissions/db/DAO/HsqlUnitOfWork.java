package permissions.db.DAO;

import permissions.db.UOW.UnitOfWork;
import permissions.db.common.SQLOperations;
import permissions.db.repositories.UnitOfWorkRepository;
import permissions.domain.Entity;
import permissions.domain.EntityState;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Michal on 27/11/2015.
 */
public class HsqlUnitOfWork extends SQLOperations implements UnitOfWork {

    private Map<Entity, UnitOfWorkRepository> entities = new LinkedHashMap<Entity, UnitOfWorkRepository>();

    public HsqlUnitOfWork(){
        super();
        try {
            connection.setAutoCommit(false);
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
        }
    }

    public void saveChanges() {
        for (Entity entity: entities.keySet()){
            takeAction(entity);
        }
        try {
            connection.commit();
            entities.clear();
        }catch (SQLException sqlerr){
            sqlerr.printStackTrace();
        }
    }

    private void takeAction(Entity entity){
        switch (entity.getState()) {
            case NEW:
                entities.get(entity).persistAdd(entity);
                break;
            case MODIFIED:
                entities.get(entity).persistUpdate(entity);
                break;
            case UNCHANGED:
                break;
            case DELETED:
                entities.get(entity).persistDelete(entity);
                break;
            case UNKNOWN:
                break;
        }
    }

    public void undo() {
        entities.clear();
    }

    public void markAsNew(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.NEW);
        entities.put(entity, repository);
    }

    public void markAsDeleted(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.DELETED);
        entities.put(entity, repository);
    }

    public void markAsChanged(Entity entity, UnitOfWorkRepository repository) {
        entity.setState(EntityState.MODIFIED);
        entities.put(entity, repository);
    }
}
