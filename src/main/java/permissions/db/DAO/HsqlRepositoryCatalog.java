package permissions.db.DAO;

import java.sql.Connection;

import permissions.db.DAO.*;
import permissions.db.catalogs.RepositoryCatalog;
import permissions.db.repositories.*;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

	Connection connection;
	
	public PersonRepository people() {
		return new HsqlPersonRepository();
	}
	public UserRepository user() { return new HsqlUserRepository(); }
	public PermissionRepository permission() { return new HsqlPermissionRepository(); }
	public AddressRepository address() { return new HsqlAddressRepository(); }
	public RolesRepository roles() { return new HsqlRolesRepository(); }

	public EnumerationValueRepository enumeration() {
		return null;
	}

	public UserRepository users() {
		return null;
	}

}
