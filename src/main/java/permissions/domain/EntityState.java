package permissions.domain;

/**
 * Created by Michal on 27/11/2015.
 */
public enum EntityState {
    NEW, MODIFIED, UNCHANGED, DELETED, UNKNOWN;
}
