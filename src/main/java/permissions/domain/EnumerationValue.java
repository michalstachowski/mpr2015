package permissions.domain;

import permissions.domain.Entity;

/**
 * Created by Michal on 27/11/2015.
 */
public class EnumerationValue extends Entity {
    public int getIntkey() {
        return intkey;
    }

    public void setIntkey(int intkey) {
        this.intkey = intkey;
    }

    public String getStringKey() {
        return stringKey;
    }

    public void setStringKey(String stringKey) {
        this.stringKey = stringKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEnumerationName() {
        return enumerationName;
    }

    public void setEnumerationName(String enumerationName) {
        this.enumerationName = enumerationName;
    }

    protected int intkey;
    protected String stringKey;
    protected String value;
    protected String enumerationName;
}
