package permissions.domain;

/**
 * Created by Michal on 27/11/2015.
 */
public abstract class Entity extends DomainBase{
    protected EntityState state;

    public EntityState getState() {
        return state;
    }
    public void setState(EntityState state) {
        this.state = state;
    }
}
