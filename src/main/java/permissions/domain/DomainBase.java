package permissions.domain;

/**
 * Created by Michal on 27/11/2015.
 */
public abstract class DomainBase {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
