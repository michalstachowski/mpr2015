package permissions.domain;

import permissions.domain.Entity;

/**
 * Created by Michal on 27/11/2015.
 */
public class UserRoles extends Entity {

    protected int userId;
    protected int roleId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }



}
