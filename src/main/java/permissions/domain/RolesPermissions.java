package permissions.domain;

import permissions.domain.Entity;

/**
 * Created by Michal on 27/11/2015.
 */
public class RolesPermissions extends Entity {
    protected int roleId;
    protected int permissionId;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }
}
